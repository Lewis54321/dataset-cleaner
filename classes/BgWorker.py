from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

import traceback


class WorkerClass(QObject):

    finished = pyqtSignal(str, str)
    error = pyqtSignal(tuple)
    update = pyqtSignal(str)

    def __init__(self, fn, *args):

        super(self.__class__, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args

    @pyqtSlot()
    def run_function(self):

        try:
            stage, fold = self.fn(*self.args)
            self.finished.emit(stage, fold)

        except BaseException as exception:
            traceback.print_exc()
            self.error.emit(exception)
