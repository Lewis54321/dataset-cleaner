# DATASET CLEANING

## OVERVIEW

Gui developed for cleaning data stored either in pickle or database format with following operations:

- Deal with missing data (NaN) by replacing with a particular value throughout a column
- Replace a particular category label present in a column with another label
- Convert the value type of a column to another value type

## DEPENDENCIES

- PyQt5 (version 5.9.2 tested)
- sqlalchemy (version 1.3.5 tested)
- numpy (version 1.16.4 tested)
- pandas (version 0.24.2 tested)
