from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QFileDialog

from window import Ui_MainWindow
from classes.BgWorker import WorkerClass
from sqlalchemy import create_engine
import sys
import os
import datetime
import numpy as np
import math
import pandas as pd
import pickle
from functools import partial


def exception_hook(exctype, value, traceback):
    _excepthook(exctype, value, traceback)
    sys.exit(1)


_excepthook = sys.excepthook
sys.excepthook = exception_hook


class Main(QtWidgets.QMainWindow):

    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        # Set up signals
        self.set_up_signals()

        # Gets the current working directory
        self._cwd = os.getcwd()
        self._ui.lineEditExpDir.setText(os.path.join(self._cwd, 'export'))

        # Get the current date and time
        self._dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")

        # Setting up the background thread and background worker
        self._thread = QThread()
        self._worker = None

        # Import Directories
        self._pickleDir = None
        self._textDir = None

        # Main data containers
        self._dataset = dict()
        self._mainFold = None
        self._actionsList = list()
        self._actionsList.append('Operations performed on: %s' % self._dateTime)
        self._actionsList.append('')

    def set_up_signals(self):
        """
        Method for setting up all the widget signals when the form loads
        """

        def exclusive_groupbox(thisType, groupList, typeList, tog):

            for group, type in zip(groupList, typeList):
                if thisType == type:
                    group.setChecked(True)
                else:
                    group.setChecked(False)

        def update_data_type(lineEdit, comboBox, columnIdx):

            column = comboBox.currentText()
            if column == 'all':
                lineEdit.setText('unknown')
                return
            subType = self._dataset[self._mainFold][column].dtype
            lineEdit.setText(str(subType))

        def update_missing_value(replace_method):

            if replace_method == 'Value':
                self._ui.lineEditMissValue.setEnabled(True)
            else:
                self._ui.lineEditMissValue.setDisabled(True)

        varTypeList = ['obj', 'float', 'int', 'bool', 'datetime', 'timedelta', 'category']

        # IMPORT
        groupList = [self._ui.groupBoxImportSql, self._ui.groupBoxImportPickle]
        impTypeList = ['mysql', 'pickle']
        self._ui.groupBoxImportSql.clicked.connect(partial(exclusive_groupbox, 'mysql', groupList, impTypeList))
        self._ui.groupBoxImportPickle.clicked.connect(partial(exclusive_groupbox, 'pickle', groupList, impTypeList))
        self._ui.pushButtonImportSql.clicked.connect(partial(self.get_import_data, 'mysql'))
        self._ui.pushButtonImportPickle.clicked.connect(partial(self.get_import_data, 'pickle'))
        self._ui.pushButtonSelectImpDir.clicked.connect(self.get_pickle_import_dir)

        # MISSING DATA
        self._ui.pushButtonMissing.clicked.connect(self.get_replace_missing)
        self._ui.comboBoxMissType.currentTextChanged.connect(update_missing_value)
        self._ui.pushButtonHead.clicked.connect(partial(self.get_print_head, 'missing', self._ui.comboBoxMissCol, None, None))

        # REPLACE CATEGORICAL DATA
        self._ui.comboBoxExistingCol.currentTextChanged.connect(self.update_combobox_with_column)
        self._ui.pushButtonReplace.clicked.connect(self.get_replace_categorical)
        self._ui.pushButtonReplaceHead.clicked.connect(partial(self.get_print_head, 'replace', self._ui.comboBoxExistingCol, None, None))

        # CONVERT DATA TYPE
        groupList = [self._ui.groupBoxObjConvert, self._ui.groupBoxFloatConvert, self._ui.groupBoxIntConvert,
                     self._ui.groupBoxBoolConvert, self._ui.groupBoxDatetimeConvert,
                     self._ui.groupBoxTimedeltaConvert, self._ui.groupBoxCategoryConvert]
        comboList = [self._ui.comboBoxObjConvert, self._ui.comboBoxFloatConvert, self._ui.comboBoxIntConvert,
                     self._ui.comboBoxBoolConvert, self._ui.comboBoxDatetimeConvert,
                     self._ui.comboBoxTimedeltaConvert, self._ui.comboBoxCategoryConvert]
        lineList = [self._ui.lineEditObjType, self._ui.lineEditFloatType, self._ui.lineEditIntType,
                    self._ui.lineEditBoolType, self._ui.lineEditDatetimeType, self._ui.lineEditTimedeltaType,
                    self._ui.lineEditCategoryType]
        for group, type in zip(groupList, varTypeList):
            group.clicked.connect(partial(exclusive_groupbox, type, groupList, varTypeList))
        for combo, lineEdit in zip(comboList, lineList):
            combo.activated.connect(partial(update_data_type, lineEdit, combo))
        self._ui.pushButtonConvert.clicked.connect(partial(self.get_convert, groupList, comboList))
        self._ui.pushButtonHeadConvert.clicked.connect(partial(self.get_print_head, 'convert', None, comboList, groupList))

        # EXPORT
        self._ui.pushButtonSelectExpDir.clicked.connect(self.get_pickle_export_dir)
        self._ui.pushButtonExportPickle.clicked.connect(self.get_export_data)

    # IMPORT
    @pyqtSlot()
    def get_pickle_import_dir(self):
        """
        Button triggered method for selecting the pickle export directory
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setNameFilter("Data File (*.pickle)")
        dlg.setDirectory(os.path.dirname(os.path.dirname(self._cwd)))

        if dlg.exec_():
            dataDir = dlg.selectedFiles()[0]

        try:
            self._ui.lineEditImpDir.setText(dataDir)
            self._ui.statusbar.showMessage('Directory Selected')
        except BaseException as exception:
            self._ui.statusbar.showMessage('Could not update directory')
            raise exception

    @pyqtSlot()
    def get_import_data(self, dataType):
        """
        Button triggered method for importing data from the database
        """

        fold = self._ui.comboBoxImportFold.currentText()
        if dataType == 'mysql':
            if self._ui.lineEditTableName.text() == '' or self._ui.lineEditSchemaName.text() == '':
                return
            self._worker = WorkerClass(self.import_sql_worker, fold, self._ui.lineEditTableName.text(),
                                       self._ui.lineEditSchemaName.text(), self._ui.lineEditPassword.text())
        elif dataType == 'pickle':
            if self._ui.lineEditImpDir.text() == '':
                return
            self._worker = WorkerClass(self.import_pickle_worker, fold, self._ui.lineEditImpDir.text())

        self._worker.update.connect(self.update_message)
        self._worker.finished.connect(self.thread_finished)
        self._worker.moveToThread(self._thread)
        self._thread.started.connect(self._worker.run_function)
        self._thread.start()
        self._ui.progressBarImport.setValue(0)

        self._ui.statusbar.showMessage('Importing Data...')

    def import_sql_worker(self, fold, tableName, schemaName, password):
        """
        Worker for importing table from the mySQL database
        :param tableName: String [Name of the table to be imported from]
        :param schemaName: String [Name of the schema to be imported from]
        :param password: String [Database password]
        :return: (String, String)
        """

        # Set up the connection object
        engine = create_engine('mysql+pymysql://root:%s@localhost/%s' % (password, schemaName), pool_recycle=3600)
        dbConnection = engine.connect()
        self._worker.update.emit('Successfully connected to the MySQL database')
        self._dataset[fold] = pd.read_sql('SELECT * FROM %s.%s' % (schemaName, tableName), dbConnection, index_col=None)

        return 'import', fold

    def import_pickle_worker(self, fold, dir):

        self._dataset[fold] = pd.read_pickle(dir)
        if not isinstance(self._dataset[fold], pd.DataFrame):
            raise Exception('Error importing pickle datafile. Pickle data not DataFrame type')

        return 'import', fold

    # CLEAN
    @pyqtSlot()
    def get_print_head(self, stage, comboBox=None, comboList=None, groupList=None):

        if stage == 'convert':
            comboBox = [combo for combo, group in zip(comboList, groupList) if group.isChecked()][0]
        column = comboBox.currentText()
        if column == '' or column == 'all':
            self._ui.statusbar.showMessage('Please select a unique column')
            return
        dtype = self._dataset[self._mainFold][column].dtype
        headList = self._dataset[self._mainFold][column].head(n=20).to_string(header=False, index=False).split('\n')

        if stage == 'missing':
            self._ui.listWidgetMiss.addItem((column + ' (%s): ' % dtype) + ', '.join(value for value in headList))
        elif stage == 'replace':
            self._ui.listWidgetReplace.addItem((column + ' (%s): ' % dtype) + ', '.join(value for value in headList))
        elif stage == 'convert':
            self._ui.listWidgetConvert.addItem((column + ' (%s): ' % dtype) + ', '.join(value for value in headList))

    @pyqtSlot()
    def get_replace_missing(self):
        """
        Button triggered method for replacing missing (NaN) data
        :return:
        """

        column = self._ui.comboBoxMissCol.currentText()
        if column == '' or (self._ui.comboBoxMissType.currentText() == 'Value' and self._ui.lineEditMissValue.currentText() == ''):
            self._ui.statusbar.showMessage('Please enter all values before running')
            return
        elif column == 'all' and self._ui.comboBoxMissType.currentText() == 'Value':
            self._ui.statusbar.showMessage('This configuration is not currently supported')
            return

        if column == 'all':
            headings = [self._ui.comboBoxMissCol.itemText(i) for i in range(self._ui.comboBoxMissCol.count())]
            headings.remove('all')
        else:
            headings = column
            headType = self._dataset[self._mainFold][column].dtype

        # Insert new values of the type of the column has a definite data type, otherwise will input string
        operation = self._ui.comboBoxMissType.currentText()
        if operation == 'Value':
            value = self._ui.lineEditMissValue.currentText()
            if headType == 'int':
                value = int(value)
            elif headType == 'float':
                value = float(value)
            elif headType == 'bool':
                value = bool(value)
            elif headType.name == 'category':
                self._ui.statusbar.showMessage('Category replacement not currently supported')
                return
            elif headType == 'datetime64[ns]':
                value = pd.Timestamp(value)
            elif headType == 'timedelta64[ns]':
                value = pd.Timedelta(value)
            self._dataset[self._mainFold][headings].fillna(value=value, inplace=True)
            if 'Test' in self._dataset.keys():
                self._dataset['Test'][headings].fillna(value=value, inplace=True)
        elif operation == 'Drop':
            self._dataset[self._mainFold] = self._dataset[self._mainFold][self._dataset[self._mainFold][headings].notna()]
            if 'Test' in self._dataset.keys():
                self._dataset['Test'] = self._dataset['Test'][self._dataset[self._mainFold][headings].notna()]
        elif operation == 'Interpolate':
            self._dataset[self._mainFold][headings].interpolate(inplace=True)
            if 'Test' in self._dataset.keys():
                self._dataset['Test'][headings].interpolate(inplace=True)

        self.listwidget_update('Missing data removed via operation (%s) from column: %s: %s'
                               % (operation, column, self._dataset[self._mainFold][column].dtype), 'missing')
        self._ui.statusbar.showMessage('Missing Data Replaced', 3000)
        self._ui.progressBarMissing.setValue(100)
        self._ui.comboBoxMissCol.removeItem(self._ui.comboBoxMissCol.currentIndex())

    @pyqtSlot()
    def get_replace_categorical(self):

        column = self._ui.comboBoxExistingCol.currentText()
        repString = self._ui.comboBoxReplace.currentText().strip('\"')
        newString = self._ui.lineEditReplace.text()
        if column == '' or repString == '' or newString == '':
            return

        self._dataset[self._mainFold][column].replace(repString, newString, inplace=True)
        if 'Test' in self._dataset.keys():
            self._dataset['Test'][column].replace(repString, newString, inplace=True)
        self.listwidget_update('Column %s: Category string "%s" replaced with string "%s"'
                               % (column, repString, newString), 'replace')
        self._ui.statusbar.showMessage('Category Data Replaced', 3000)
        self.update_combobox_with_column(column)
        self._ui.progressBarReplace.setValue(100)

    @pyqtSlot()
    def get_convert(self, groupList, comboList):

        combo = [combo for combo, group in zip(comboList, groupList) if group.isChecked()][0]
        column = combo.currentText()
        if column == '':
            self._ui.statusbar.showMessage('Please select a column')
            return
        if column == 'all':
            oldType = 'unknown'
            headings = [combo.itemText(i) for i in range(combo.count())]
            headings.remove('all')
        else:
            oldType = self._dataset[self._mainFold][column].dtype
            headings = column
        newType = self._ui.comboBoxNewType.currentText()

        self._dataset[self._mainFold][headings] = self._dataset[self._mainFold][headings].astype(newType)
        if 'Test' in self._dataset.keys():
            self._dataset['Test'][headings] = self._dataset['Test'][headings].astype(newType)

        self.listwidget_update('Column [%s]: type "%s" converted to type "%s"' % (column, oldType, newType), 'convert')
        self._ui.statusbar.showMessage('Type conversion completed', 3000)
        self._ui.progressBarReplace.setValue(100)
        self.update_type_combo_boxes(self.get_type_distribution(self._dataset[self._mainFold]))

    # EXPORT
    @pyqtSlot()
    def get_pickle_export_dir(self):
        """
        Button triggered method for selecting the pickle export directory
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        dlg.setDirectory(self._ui.lineEditExpDir.text())

        if dlg.exec_():
            fileName = os.path.basename(self._ui.lineEditImpDir.text())
            baseCalDir = dlg.selectedFiles()[0]

        try:
            self._ui.lineEditExpDir.setText(os.path.join(baseCalDir, fileName))
            self._ui.statusbar.showMessage('Directory Selected')
        except BaseException as exception:
            self._ui.statusbar.showMessage('Could not update directory')

    @pyqtSlot()
    def get_export_data(self):
        """
        Button triggered method for exporting data from the database
        """

        if self._ui.lineEditExpDir.text() == '':
            return
        self._worker = WorkerClass(self.export_pickle_worker, self._ui.lineEditExpDir.text())

        self._worker.update.connect(self.update_message)
        self._worker.finished.connect(self.thread_finished)
        self._worker.moveToThread(self._thread)
        self._thread.started.connect(self._worker.run_function)
        self._thread.start()
        self._ui.progressBarExport.setValue(0)

        self._ui.statusbar.showMessage('Exporting Data...')

    def export_pickle_worker(self, direct):

        subDir = os.path.join(direct, self._dateTime)
        os.mkdir(subDir)

        for key in self._dataset.keys():
            self._dataset[key].to_pickle(os.path.join(subDir, 'cleanData_' + key + '.pickle'))

        with open(os.path.join(subDir, 'operations.text'), 'w') as f:
            for op in self._actionsList:
                f.write("%s\n" % op)

        return 'export', None

    # DATA UPDATE
    def update_combobox_with_column(self, column):

        self._ui.comboBoxReplace.clear()
        self._ui.comboBoxReplace.addItems(
            sorted(['"' + cat + '"' for cat in self._dataset[self._mainFold][column].dropna().unique()]))

    def update_fold_type_combo_box(self, fold):

        self._ui.comboBoxImportFold.clear()

        if fold == 'Combined':
            self._mainFold = 'Combined'
            return True

        if (fold == 'Train') and ('Test' not in self._dataset.keys()):
            self._ui.comboBoxImportFold.addItem('Test')
            self._mainFold = 'Train'
            return True
        elif (fold == 'Test') and ('Train' not in self._dataset.keys()):
            self._ui.comboBoxImportFold.addItem('Train')
            self._mainFold = 'Train'
            return False

    @staticmethod
    def get_type_distribution(data):

        allTypeDict = {'object': ['object'], 'float': ['float32', 'float64'], 'int': ['int32', 'int64'],
                       'bool': ['bool'],
                       'datetime': ['datetime'], 'timedelta': ['timedelta'], 'category': ['category']}
        typeDict = dict()
        for dType in allTypeDict.keys():
            typeDict[dType] = list(data.select_dtypes(include=allTypeDict[dType]).columns)
        typeDict['unknown'] = list(data.select_dtypes(
            exclude=[item for sublist in list(allTypeDict.values()) for item in sublist]).columns)

        return typeDict

    def update_type_combo_boxes(self, typeDict):

        self._ui.comboBoxExistingCol.addItems(typeDict['object'] + typeDict['category'])

        if len(typeDict['object']) != 0:
            self._ui.comboBoxObjConvert.clear()
            self._ui.comboBoxObjConvert.addItems(['all'] + typeDict['object'])
        if len(typeDict['float']) != 0:
            self._ui.comboBoxFloatConvert.clear()
            self._ui.comboBoxFloatConvert.addItems(['all'] + typeDict['float'])
        if len(typeDict['int']) != 0:
            self._ui.comboBoxIntConvert.clear()
            self._ui.comboBoxIntConvert.addItems(['all'] + typeDict['int'])
        if len(typeDict['bool']) != 0:
            self._ui.comboBoxBoolConvert.clear()
            self._ui.comboBoxBoolConvert.addItems(['all'] + typeDict['bool'])
        if len(typeDict['datetime']) != 0:
            self._ui.comboBoxDatetimeConvert.clear()
            self._ui.comboBoxDatetimeConvert.addItems(['all'] + typeDict['datetime'])
        if len(typeDict['timedelta']) != 0:
            self._ui.comboBoxTimedeltaConvert.clear()
            self._ui.comboBoxTimedeltaConvert.addItems(['all'] + typeDict['timedelta'])
        if len(typeDict['category']) != 0:
            self._ui.comboBoxCategoryConvert.clear()
            self._ui.comboBoxCategoryConvert.addItems(['all'] + typeDict['category'])

    @staticmethod
    def get_miss_columns(data):

        missColumnList = ['all'] + data.columns[data.isna().any()].tolist()
        return missColumnList

    def update_miss_combo_box(self, missColumnList):

        if len(missColumnList) > 2:
            self._ui.comboBoxMissCol.addItems(missColumnList)

    def listwidget_update(self, message, stage):

        if stage == 'import':
            self._ui.listWidgetImportDesc.addItem(message)
        elif stage == 'missing':
            self._ui.listWidgetMiss.addItem(message)
        elif stage == 'replace':
            self._ui.listWidgetReplace.addItem(message)
        elif stage == 'convert':
            self._ui.listWidgetConvert.addItem(message)

        self._actionsList.append(message)

    # WORKER SIGNALS
    def update_message(self, message):

        self._ui.listWidgetImportDesc.addItem(message)

    def thread_finished(self, stage, fold):
        """
        Thread Finished method called by the background thread to indicate that the dataset has been imported
        :param stage: String [Stage of processing, i.e.
        """

        def update_data_descriptor(typeDict, numMissCol, data):

            self._ui.listWidgetImportDesc.addItem('Number of Rows: %d' % (data.shape[0]))
            self._ui.listWidgetImportDesc.addItem('Number of Columns: %d' % (data.shape[1]))
            self._ui.listWidgetImportDesc.addItem('Column Distribution: %s' % str({k: len(v) for (k, v) in typeDict.items()}))
            self._ui.listWidgetImportDesc.addItem('Number of Columns with Missing Data: %d' % numMissCol)

        if stage == 'import':
            self.listwidget_update('Successfully imported dataset (%s)' % fold, 'import')
            self._ui.statusbar.showMessage('Import Complete')
            typeDict = self.get_type_distribution(self._dataset[fold])
            missColumnList = self.get_miss_columns(self._dataset[fold])
            update_data_descriptor(typeDict, len(missColumnList) - 1, self._dataset[fold])

            # Main fold will either be the training data or the combined data
            if self.update_fold_type_combo_box(fold):
                self.update_type_combo_boxes(typeDict)
                self.update_miss_combo_box(missColumnList)
            self._ui.progressBarImport.setValue(100)

        elif stage == 'export':
            self._ui.listWidgetExportDesc.addItem('Successfully exported dataset')
            self._ui.statusbar.showMessage('Export Complete')

            self._ui.progressBarExport.setValue(100)

        self._thread.quit()


if __name__ == "__main__":

    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())


